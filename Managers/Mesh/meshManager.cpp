#include "meshManager.h"
#include "../../Algorithms/giftWrapper.h"
#include <exception>
#include <sstream>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <omp.h>

Managers::Meshes::MeshManager::MeshManager(int xVMin, int xVMax, int yVMin, int yVMax) :
        _xVMin((float) xVMin),
        _xVMax((float) xVMax),
        _yVMax((float) yVMax),
        _yVMin((float) yVMin) {
    this->_textureUtil = std::make_shared<Utilities::Textures::TextureUtil>();
}

void Managers::Meshes::MeshManager::loadMesh(const std::string &dir) {
    // open file
    std::ifstream file;
    file.open(dir);

    this->_meshBlocks = std::make_shared<std::vector<std::shared_ptr<MeshBlock>>>();

    if (file.is_open()) {
        std::cout << "Successfully loaded mesh file\n";
    } else {
        std::cout << "Failed to open the file!\n";
        throw std::exception();
    }

    size_t lineNo = 0, pointNo{0};
    std::shared_ptr<MeshBlock> block = std::make_shared<MeshBlock>(pointNo);

    while (!file.eof()) {
        lineNo++;

        std::string line;
        getline(file, line);

        if (lineNo < 3)
            continue;

        if (line == "closed") {
            // add the current block, then create a new one
            this->_meshBlocks->push_back(block);
            block = std::make_shared<MeshBlock>(pointNo);
            continue;
        }
        if (line == "end")
            continue;

        std::vector<PRECISION> lineData;
        for (const auto &var : Managers::Meshes::MeshManager::splitLine(line, ' ')) {
            lineData.push_back(this->convertToType(var));
        }

        // Add the lines to the block
        block->addLineData(lineData);
        pointNo += lineData.size() / 2; // Increment the point sizes
    }

    file.close();
}

std::vector<std::string> Managers::Meshes::MeshManager::splitLine(const std::string &line, const char &token) {

    std::vector<std::string> strings;
    std::stringstream stream(line);
    std::string intermediate;

    // Tokenizing w.r.t. space ' '
    while (getline(stream, intermediate, token)) {
        strings.push_back(intermediate);
    }

    return strings;
}

PRECISION Managers::Meshes::MeshManager::convertToType(const std::string &value) {
    return CONVERT_TO_PRECISION(value);
}

std::shared_ptr<std::vector<Vertex>> Managers::Meshes::MeshManager::getFlattenedMesh(int idx) {
    std::shared_ptr<std::vector<Vertex>> vertices = std::make_shared<std::vector<Vertex>>();
    auto mesh = this->_meshBlocks->at(idx)->getLines();

    for (auto line : *mesh) {
        vertices->insert(vertices->end(), line.begin(), line.end());
    }

    return vertices;
}

void Managers::Meshes::MeshManager::processMesh() {
    this->_renderableMesh = std::make_shared<std::vector<std::vector<Vertex>>>();
    this->_stateSpace = std::make_shared<std::vector<Vertex>>();

#pragma omp parallel for default(none) shared(_strips, _meshBlocks) // Read all blocks in parallel
    for (int i = 0; i < this->_meshBlocks->size(); ++i) {
        auto lines = (*this->_meshBlocks)[i]->getLines();

#pragma omp critical
        {
            // finds the bounding
            if ((*this->_meshBlocks)[i]->getBoundingBox()._xMax > this->_xWMax)
                this->_xWMax = (*this->_meshBlocks)[i]->getBoundingBox()._xMax;

            if ((*this->_meshBlocks)[i]->getBoundingBox()._xMin < this->_xWMin)
                this->_xWMin = (*this->_meshBlocks)[i]->getBoundingBox()._xMin;

            if ((*this->_meshBlocks)[i]->getBoundingBox()._yMax > this->_yWMax)
                this->_yWMax = (*this->_meshBlocks)[i]->getBoundingBox()._yMax;

            if ((*this->_meshBlocks)[i]->getBoundingBox()._yMin < this->_yWMin)
                this->_yWMin = (*this->_meshBlocks)[i]->getBoundingBox()._yMin;

            this->_renderableMesh->reserve(lines->size() + this->_renderableMesh->size());
            this->_renderableMesh->insert(this->_renderableMesh->end(), lines->begin(), lines->end());

            for (auto l : *lines) {
                this->_stateSpace->insert(this->_stateSpace->end(), l.begin(), l.end());
            }
        }
    }

    this->_strips = std::make_shared<std::vector<std::shared_ptr<Strip>>>();

    int stripIdx{0};
    for (int j = 0; j < this->_meshBlocks->size(); ++j) {
        auto lines = (*this->_meshBlocks)[j]->getLines();
        auto strip = createStrips(lines, stripIdx);

        // Reserve more memory and insert the strip
        this->_strips->reserve(this->_strips->size() + strip.size());
        // Add the strip to the strips vector
        this->_strips->insert(this->_strips->end(), strip.begin(), strip.end());
    }

}

void
Managers::Meshes::MeshManager::performWindowTransform(const std::shared_ptr<std::vector<std::vector<Vertex>>> &mesh) {
    // Calculate the scaling factor
    float scalingX = (float) (this->_xVMax - this->_xVMin) / (this->_xWMax - this->_xWMin);
    float scalingY = (float) (this->_yVMax - this->_yVMin) / (this->_yWMax - this->_yWMin);

    // Translate to local origin
    auto transToOrigin = glm::translate(glm::mat4(1.f),
                                        glm::vec3(this->_xWMin * -1, this->_yWMin * -1, 0));
    // Scale to fit viewport
    auto scale = glm::scale(glm::mat4(1.f),
                            glm::vec3(scalingX, scalingY, 0));
    // Translate to viewport origin
    auto transToViewport = glm::translate(glm::mat4(1.f),
                                          glm::vec3(this->_xVMin, this->_yVMin, 0));

    // Create the transformation matrix
    auto result = transToViewport * scale * transToOrigin;

    // Transform every point
    for (int i = 0; i < mesh->size(); ++i) {
        for (auto &v : (*mesh)[i]) {
            auto point = v.Position;
            v.Position = result * glm::vec4(point, 1);
        }
    }
}

std::vector<std::shared_ptr<Strip>>
Managers::Meshes::MeshManager::createStrips(const std::shared_ptr<std::vector<std::vector<Vertex>>> &mesh,
                                            int &stripIdx) {
    std::vector<std::shared_ptr<Strip>> strips;
    strips.reserve(mesh->size());

    // Perform window transform on the mesh
    this->performWindowTransform(mesh);

    for (int j = 0; j < mesh->size() - 1; ++j) {
        int eboIdx = 0;
        auto v0 = std::make_shared<std::vector<Vertex>>((*mesh)[j]);
        auto v1 = std::make_shared<std::vector<Vertex>>((*mesh)[j + 1]);

        auto vertices = std::make_shared<std::vector<Vertex>>();
        // Create some space for the vertex
        vertices->reserve(v0->size() + v1->size());

        for (int i = 0; i < v0->size(); ++i) {
            (*v0)[i].myNumber = eboIdx; // Set the vertex index
            vertices->push_back((*v0)[i]); // Add the vertex to the collection

            ++eboIdx;
        }

        // Vertex
        for (int k = 0; k < v1->size(); ++k) {
            (*v1)[k].myNumber = eboIdx;
            vertices->push_back((*v1)[k]);

            ++eboIdx;
        }

        auto s = this->generateStripStructures(v0, v1, vertices, stripIdx);
        s->_textureUtil = this->_textureUtil;
        // Insert the strip made in this iteration
        strips.push_back(s);
        stripIdx++;
    }

    return strips;
}

std::shared_ptr<Strip>
Managers::Meshes::MeshManager::generateStripStructures(const std::shared_ptr<std::vector<Vertex>> &v0,
                                                       const std::shared_ptr<std::vector<Vertex>> &v1,
                                                       const std::shared_ptr<std::vector<Vertex>> &stripVertices,
                                                       int stripIdx) {

    auto indices = std::make_shared<std::vector<int>>();
    indices->reserve(v0->size() * 5); // Reserve memory for vector

    auto cells = std::make_shared<std::vector<Cell>>();
    cells->reserve(v0->size());

    assert(v0->size() == v1->size());

    for (int i = 0; i < v0->size() - 1; ++i) {
        // creates the cell
        auto v = std::make_shared<std::vector<Vertex>>();
        v->push_back((*v0)[i]);
        v->push_back((*v0)[i + 1]);
        v->push_back((*v1)[i + 1]);
        v->push_back((*v1)[i]);
        cells->push_back(Cell(v, i));

        // add cell the indices
        indices->push_back((*v0)[i].myNumber);
        indices->push_back((*v0)[i + 1].myNumber);
        indices->push_back((*v1)[i + 1].myNumber);
        indices->push_back((*v1)[i].myNumber);
        indices->push_back((*v0)[i].myNumber); // extra vertex will close the polygon
    }

    // Create the strip
    auto strip = std::make_shared<Strip>(stripVertices, indices, stripIdx);
    // Create the texture for the strip
    strip->_textureId = this->_textureUtil->generateTexture(cells, strip);
    // Create the texture uvs for the strip
    strip->_textureUv = this->_textureUtil->paintStripWithTexture(strip);
    return strip;
}

