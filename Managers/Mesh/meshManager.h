#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <fstream>
#include <vector>
#include <string>
#include "../../Utilities/MiindTypes/vertex.h"
#include "../../Utilities/MiindTypes/strip.h"
#include "../../Utilities/Textures/textureUtil.h"
#include "../../Utilities/MiindTypes/meshBlock.h"
#include "../../defines.h"

namespace Managers::Meshes {
    class MeshManager {
    public:
        explicit MeshManager(int xVMin, int xVMax, int yVMin, int yVMax);

        // Reads the mesh file
        void loadMesh(const std::string &dir);

        // Retrieves the mesh at the given index as a vector of vertices
        std::shared_ptr<std::vector<Vertex>> getFlattenedMesh(int idx);

        // Performs a window transformation on the given points.
        void performWindowTransform(const std::shared_ptr<std::vector<std::vector<Vertex>>> &mesh);

        // Processes the mesh to translate to screen coordinates
        void processMesh();

        // Creates a strip from the given mesh
        std::vector<std::shared_ptr<Strip>>
        createStrips(const std::shared_ptr<std::vector<std::vector<Vertex>>> &mesh, int &stripIdx);

        // Retrieves the processed mesh
        std::shared_ptr<std::vector<std::shared_ptr<Strip>>> getStrips() {
            return this->_strips;
        }

        // Retrieves the state space
        std::shared_ptr<std::vector<Vertex>> getState() {
            return this->_stateSpace;
        }

        std::shared_ptr<std::vector<std::vector<Vertex>>> _renderableMesh{};

    private:
        std::shared_ptr<Utilities::Textures::TextureUtil> _textureUtil{};

        std::shared_ptr<std::vector<Vertex>> _stateSpace{nullptr};

        std::shared_ptr<std::vector<std::shared_ptr<Strip>>> _strips{nullptr};

        float _xVMin{-1}, _xVMax{-1},
                _yVMin{-1}, _yVMax{-1};

        float _xWMin{INT_MAX}, _xWMax{INT_MIN},
                _yWMin{INT_MAX}, _yWMax{INT_MIN};

        // Converts the string value into precision type
        PRECISION convertToType(const std::string &value);

        /**
         * Generates all strip structures including cell indices and textures
         * @param v0 The v0 line
         * @param v1 The v1 line
         * @param stripVertices The vertices making up a strip
         * @param stripIdx The strip index
         * @return
         */
        std::shared_ptr<Strip>
        generateStripStructures(const std::shared_ptr<std::vector<Vertex>> &v0,
                                const std::shared_ptr<std::vector<Vertex>> &v1,
                                const std::shared_ptr<std::vector<Vertex>>& stripVertices,
                                int stripIdx);

        // function to split mesh file line to separate
        static std::vector<std::string> splitLine(const std::string &line, const char &token);

        std::shared_ptr<std::vector<std::shared_ptr<MeshBlock>>> _meshBlocks{nullptr};
    };
}

#endif // FILEMANAGER_H
