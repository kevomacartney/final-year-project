#version 400
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;
out float area;

float calcPrimitiveArea(vec4 polys[3]){
    float result = 0.f;

    for (int i = 0; i < 2; ++i){
        vec4 p1 = polys[i];
        vec4 p2 = polys[i+1];

        float left  = p1.x * p2.y;
        float right = p2.x * p1.y;
        result += (left - right);
    }

    return abs(result * 0.5f);
}

void main() {
    // initialize points array
    vec4 points[3] = vec4[3](gl_in[0].gl_Position,
    gl_in[1].gl_Position,
    gl_in[2].gl_Position);

    area = calcPrimitiveArea(points);
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    EndPrimitive();
}
