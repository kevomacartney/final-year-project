#version 400 core
out vec4 FragColor;
in vec2 TexCoord;

uniform sampler2D stripTexture;

void main()
{
    FragColor = texture(stripTexture, TexCoord);
}