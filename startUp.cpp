//
// Created by Kelvin Macartney on 03/02/2020.
//

#include "startUp.h"
#include <utility>
#include <vector>
#include "Algorithms/mergeSort.h"

StartUp::StartUp(std::shared_ptr<Utilities::Rendering::RenderingEngine> renderEngine, int height, int width) {
    this->_renderEngine = std::move(renderEngine);

    this->_meshRender = std::make_unique<Utilities::Rendering::MeshRenderer>();
    this->_meshManager = std::make_unique<Managers::Meshes::MeshManager>(0, width, 0, height);
    this->_voaHandler = std::make_unique<Utilities::GlObjs::VoaHandler>();
    this->_shaderUtil = std::make_shared<Utilities::Shaders::ShaderUtil>("vertexShader", "fragmentShader");
}


void StartUp::init(const std::string &mesh) {
    this->_meshManager->loadMesh(mesh); // Loads the mesh from file
    this->_meshManager->processMesh(); // Process the mesh

    this->_shaderUtil->initShaders(); // Initialise shaders
    this->_renderEngine->registerRenderer(this->_meshRender);

    this->_initialised = true;
}

void StartUp::startTheMatrix() {
    if (!this->_initialised)
        return;

    this->_meshRender->registerShaderUtil(this->_shaderUtil);
    this->_shaderUtil->activate();

    auto strips = this->_meshManager->getStrips();

    int idx{0};
    for (const auto &strip : *strips) {

//        if(idx == 376) {
            // Load vertices and indices
            auto voa = this->_voaHandler->loadData(*strip->_vertexIndices, *strip->_vertices);
            // Load the texture
            this->_voaHandler->loadTextureBufferData(voa->getId(), *strip->_textureUv);
            voa->textureId = strip->_textureId;
            voa->_textureUtil = strip->_textureUtil;

            this->_meshRender->addToRenderQueue(voa);
/*        }
        idx++;*/
    }

    this->_shaderUtil->deactivate();
}
