//
// Created by Kelvin Macartney on 16/02/2020.
//

#ifndef MIIND_GIFTWRAPPER_H
#define MIIND_GIFTWRAPPER_H

#include "../Utilities/MiindTypes/vertex.h"
#include "mergeSort.h"
#include <utility>
#include <vector>
#include <memory>
#include <stack>

typedef enum {
    Colinear,
    AntiClockwise,
    Clockwise
} PointDir;

class GiftWrapper {
public:
    explicit GiftWrapper(std::shared_ptr<std::vector<Vertex>> vertices) {
        this->_vertices = std::move(vertices);
    }

    // Scans and returns the polygons convex hull
    std::shared_ptr<std::vector<Vertex>> wrap();

private:
    std::shared_ptr<std::vector<Vertex>> _vertices{nullptr};

    int orientation(const Vertex &p, const Vertex &q, const Vertex &r);
};

std::shared_ptr<std::vector<Vertex>> GiftWrapper::wrap() {
    if (_vertices->size() < 3)
        return nullptr;

    // Find the lowest point
    int anchor{0};
    for (int j = 1; j < this->_vertices->size(); ++j) {
        if (this->_vertices->at(j).Position.y < this->_vertices->at(anchor).Position.y)
            anchor = j;
    }

    auto convexHull = std::make_shared<std::vector<Vertex>>();

    int p{anchor}, q{0};
    do {
        // Add current point to result
        convexHull->push_back(this->_vertices->at(p));

        q = (p + 1) % this->_vertices->size();

        for (int i = 0; i < this->_vertices->size(); i++) {
            if (orientation(this->_vertices->at(p), this->_vertices->at(i), this->_vertices->at(q)) == 2)
                q = i;
        }

        p = q;
    } while (p != anchor);  // While we don't come to first point

    return convexHull;
}

int GiftWrapper::orientation(const Vertex &p, const Vertex &q, const Vertex &r) {
    float val = (q.Position.y - p.Position.y) * (r.Position.x - q.Position.x) -
                (q.Position.x - p.Position.x) * (r.Position.y - q.Position.y);
    if (val == 0) return 0;     // colinear
    return (val > 0) ? 1 : 2;    // clock or counterclock wise
}

#endif //MIIND_GIFTWRAPPER_H
