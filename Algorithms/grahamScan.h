//
// Created by Kelvin Macartney on 17/02/2020.
//

#ifndef MIIND_GRAHAMSCAN_H
#define MIIND_GRAHAMSCAN_H

#include <vector>
#include <memory>
#include "../Utilities/GlObjs/vertex.h"
#include "mergeSort.h"

typedef enum {
    Cooliner,
    AntiClockwise,
    Clockwise
} Direction

class GrahamScan {
public:
    explicit GrahamScan(std::shared_ptr<std::vector<Vertex>> dataSet) {
        this->_dataset = std::move(dataSet);
        this->_mergeSort = std::make_shared<MergeSort<Vertex>>(dataSet);
    }

    template<typename isLessThanOrEqualComparator>
    std::shared_ptr<std::vector<Vertex>> scan(isLessThanOrEqualComparator comparator);

private:
    std::shared_ptr<std::vector<Vertex>> _dataset{nullptr};

    std::shared_ptr<MergeSort<Vertex>> _mergeSort{nullptr};

    // Find the lowest Y
    int findAnchor();

    // Finds the direction of r from q relative to p
    static Direction direction(glm::vec3 p, glm::vec3 q, glm::vec3 r);
};

template<typename isLessThanOrEqualComparator>
std::shared_ptr<std::vector<Vertex>> GrahamScan::scan(isLessThanOrEqualComparator comparator) {
    int anchorIdx = findAnchor();

    return std::shared_ptr<std::vector<Vertex>>();
}

Direction GrahamScan::direction(glm::vec3 p, glm::vec3 q, glm::vec3 r) {
    float result = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
    if (result == 0)
        return Cooliner;

    return result < 0 ? AntiClockwise : Clockwise;
}


int GrahamScan::findAnchor() {
    int anchor{0};
    for (int j = 1; j < this->_dataset->size(); ++j) {
        auto point = this->_dataset->at(j);
        int y = point.Position.y;

        // Minimum y coordinate and or min x
        if ((y < anchor) || ((anchor == y) && point.Position.x < this->_dataset->at(anchor).Position.x))
            anchor = j;
    }

    Vertex anchorPoint = this->_dataset->at(anchor);

    // Sorts the point with directional comparator (capture the anchorPoint)
    _mergeSort->sort([anchorPoint](Vertex q, Vertex r) {
        if(direction(anchorPoint.Position, q.Position, r.Position) == AntiClockwise)
            return true;

        //todo, figure out which direction to sort
        return false;
    });


    return 0;
}

#endif //MIIND_GRAHAMSCAN_H
