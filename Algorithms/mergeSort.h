//
// Created by Kelvin Macartney on 12/02/2020.
//

#ifndef MIIND_MERGESORT_H
#define MIIND_MERGESORT_H

#include <vector>

/**
 * Implements the merge sort algorithm
 * @tparam T1 The type being compared
 * @tparam ComputeFunc The computer function which returns a variable to be used for comparison
 */
template<typename T1>
class MergeSort {
    std::shared_ptr<std::vector<T1>> list;
public:
    explicit MergeSort(std::shared_ptr<std::vector<T1>> dataSet) {
        this->list = std::move(dataSet);
    }

    // Sorts the dataset
    template<typename isLessOrEqualComparator>
    std::shared_ptr<std::vector<T1>> sort(isLessOrEqualComparator compareCallback);

private:
    // Merges the two arrays together
    template<typename isLessOrEqualComparator>
    void merge(isLessOrEqualComparator isLessOrEqual, int l, int m, int r);

    // Sorts the dataset
    template<typename isLessOrEqualComparator>
    void sort(isLessOrEqualComparator compareCallback, int l, int r);
};

template<typename T1>
template<typename isLessOrEqualComparator>
std::shared_ptr<std::vector<T1>> MergeSort<T1>::sort(isLessOrEqualComparator compareCallback) {
    sort(compareCallback, 0, this->list->size() - 1);
    return this->list;
}

template<typename T1>
template<typename isLessOrEqualComparator>
void MergeSort<T1>::sort(isLessOrEqualComparator compareCallback, int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;

        sort(compareCallback, l, m); // Sort the left side
        sort(compareCallback, m + 1, r); // Sort the right side

        merge(compareCallback, l, m, r); // Merge the two side
    }
}

template<typename T1>
template<typename isLessOrEqualComparator>
void MergeSort<T1>::merge(isLessOrEqualComparator isLessOrEqual, int l, int m, int r) {
    int i{0}, j{0}, k{0};
    int n1 = m - l + 1;
    int n2 = r - m;

    std::vector<T1> leftTmp{}, rightTmp{};
    leftTmp.resize(n1);
    rightTmp.resize(n2);

    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++)
        leftTmp.at(i) = this->list->at(l + i);
    for (j = 0; j < n2; j++)
        rightTmp.at(j) = this->list->at(m + 1 + j);

    i = 0, j = 0, k = l;
    // Merge the temp arrays to main array
    while (i < n1 && j < n2) {
        if (isLessOrEqual(leftTmp.at(i), rightTmp.at(j))) {
            this->list->at(k) = leftTmp.at(i);
            i++;
        } else {
            this->list->at(k) = rightTmp.at(j);
            j++;
        }
        k++;
    }

    // Copies elements on the left vector
    while (i < n1) {
        this->list->at(k) = leftTmp.at(i);
        i++;
        k++;
    }

    //Copies elements on the right vector
    while (j < n2) {
        this->list->at(k) = rightTmp.at(j);
        j++;
        k++;
    }
}

#endif //MIIND_MERGESORT_H
