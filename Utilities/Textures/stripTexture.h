//
// Created by Kelvin Macartney on 10/12/2019.
//

#ifndef MIIND_PROTOTYPE_TEXTURE2D_H
#define MIIND_PROTOTYPE_TEXTURE2D_H

#include <string>
#include <map>
#include <glad/glad.h>
#include "../MiindTypes/cell.h"
#include "../MiindTypes/strip.h"

namespace Utilities::Textures {
    /**
     * Class representing a strips single texture
     */
    class StripTexture {
    public:
        /**
         * Initialises the class
         * @param texture path to the
         * @param callback
         */
        StripTexture(std::weak_ptr<std::vector<Cell>> cells, std::weak_ptr<Strip> strip);

        /**
         * Sets this texture as the active texture
         */
        void use();

        /**
         * Retrieves the OpenGL texture id
         * @return The textures openGl id
         */
        unsigned int getTextureId() { return this->_textureId; }

        /**
         * Computes a texture for this strip
         * @return The openGl texture id
         */
        unsigned int computeTexture();

        ~StripTexture() {
            // Free the raw texture data
            delete this->_rawTextureData;
            this->_rawTextureData = nullptr;

            // free the textures
            glDeleteTextures(1, &this->_textureId);
        }

    private:

        void writeToFile(std::string name, int width, int height);

        /**
         * Represents the OpenGl texture id
         */
        unsigned int _textureId{};

        /**
         * Represents the raw texture RGBA data
         */
        unsigned char *_rawTextureData{nullptr};

        /**
         * Represents the cells of the strips being represented by this texture
         */
        std::weak_ptr<std::vector<Cell>> stripCells{};

        /**
         * The strip representing this texture
         */
        std::weak_ptr<Strip> _strip{};

        /**
         * Computes the cell texture using the provided data
         * @param area The area of the cell
         * @param width The width of the texture
         * @param height The height for the texture
         * @return The texture data
         */
        unsigned char *paintTexture(int width, int height);

    };
}

#endif //MIIND_PROTOTYPE_TEXTURE2D_H
