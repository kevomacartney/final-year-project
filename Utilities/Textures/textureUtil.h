//
// Created by Kelvin Macartney on 10/12/2019.
//

#ifndef MIIND_PROTOTYPE_TEXTUREUTIL_H
#define MIIND_PROTOTYPE_TEXTUREUTIL_H

#include "stripTexture.h"
#include "../MiindTypes/vertex.h"
#include <string>
#include <memory.h>
#include <vector>
#include <map>

namespace Utilities::Textures {
    class TextureUtil {
    public:
        TextureUtil();

        /**
         * Uses the texture with the given id
         * @param tId The opengl texture id returned when texture is loaded
         */
        void activateTexture(unsigned int tId);

        /**
         * Stops using textures by setting it 0
         * @param tId The texture Id
         */
        static void deactivateTexture();

        /**
         * Generates a textures for the given strip
         * @param vertices
         * @return The texture id
         */
        unsigned int generateTexture(std::weak_ptr<std::vector<Cell>> cells,
                                     std::weak_ptr<Strip> strip);

        /**
         * Generates texture coordinates for the strip
         * @param strip The strip instance
         * @return The texture coordinates
         */
        static std::shared_ptr<std::vector<glm::vec2>> paintStripWithTexture(std::weak_ptr<Strip> strip);

        ~TextureUtil() = default;

    private:
        std::unique_ptr<std::map<int, std::shared_ptr<Textures::StripTexture>>> _textureMap{};

        /**
         * Calculates the area  of the given cell
         * @param cell The cell
         * @return The area
         */
        static float calculateArea(std::shared_ptr<std::vector<Vertex>> &cell);
    };
}

#endif //MIIND_PROTOTYPE_TEXTUREUTIL_H
