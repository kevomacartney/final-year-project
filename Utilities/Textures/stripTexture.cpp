//
// Created by Kelvin Macartney on 10/12/2019.
//
#include <utility>
#include <sstream>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <algorithm>
#include <stdio.h>
#include <png.h>
#include <glm/gtc/matrix_transform.hpp>
#include "stripTexture.h"

Utilities::Textures::StripTexture::StripTexture(std::weak_ptr<std::vector<Cell>> cells,
                                                std::weak_ptr<Strip> strip) {
    this->stripCells = std::move(cells);
    this->_strip = std::move(strip);
}


void Utilities::Textures::StripTexture::use() {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->_textureId);
}

unsigned int Utilities::Textures::StripTexture::computeTexture() {
    auto strip = this->_strip.lock();
    // Finds the width and height of the bounding box
    int x{(int) abs(strip->_boundingBox._xMax)},
            y{(int) abs(strip->_boundingBox._yMax)};

    // Compute the texture
    this->_rawTextureData = StripTexture::paintTexture(x, y);

    // Finds the width and height of the bounding box
    int width{(int) round(abs(strip->_boundingBox._xMax))},
            height{(int) round(abs(strip->_boundingBox._yMax))};

    std::string name = std::to_string(strip->_stripNo);
//    writeToFile("./textures/" + name + ".png", width, height);
//
    // Create texture and bind it
    glGenTextures(1, &this->_textureId);
    glBindTexture(GL_TEXTURE_2D, this->_textureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                 this->_rawTextureData); // sends the data to the GPU

    // Stuff to make sure the texture dispays right
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // unbind texture
    glBindTexture(GL_TEXTURE_2D, 0);

    return this->getTextureId();
}

unsigned char *Utilities::Textures::StripTexture::paintTexture(int width, int height) {
    auto strip = this->_strip.lock();

    int bytesPerPixel = 4; // number of channels
    auto tex = new unsigned char[height * width * bytesPerPixel];
    // fill the array with 0(full transparency)
    std::fill_n(tex, width * height * bytesPerPixel, 0);

    {
        // Iterate through the cells
        auto cells = this->stripCells.lock();

        for (Cell cell : *cells) {

            for (int x = (int) cell.getBoundBox()._xMin; x < (int) cell.getBoundBox()._xMax; ++x) {
                for (int y = (int) cell.getBoundBox()._yMin; y < (int) cell.getBoundBox()._yMax; ++y) {
                    {
                        // Offset to the pixel
                        auto offset = (y * width + x) * bytesPerPixel;

                        // Populate the pixel
                        tex[offset] = (int)cell._cellArea;
                        tex[offset + 1] = 0;
                        tex[offset + 2] = 0;
                        tex[offset + 3] = 255;
                    }
                }
            }
        }
        return tex;
    }
}

void Utilities::Textures::StripTexture::writeToFile(std::string name, int width, int height) {
    png_bytep row = nullptr;

    /* create file */
    FILE *fp = std::fopen(name.c_str(), "wb");
    if (!fp)
        abort();

    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING,
                                              nullptr,
                                              nullptr,
                                              nullptr);
    if (!png)
        abort();

    png_infop info = png_create_info_struct(png);
    if (!info)
        abort();

    png_init_io(png, fp);

    // Write header (8 bit colour depth)
    png_set_IHDR(png, info, width, height,
                 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png, info);

    // Allocate memory for one row (3 bytes per pixel - RGB)
    row = (png_bytep) malloc(4 * width * sizeof(png_byte));

    // Write image data
    int x, y;
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            row[x * 4] = this->_rawTextureData[(y * width + x) * 4];
            row[x * 4 + 1] = this->_rawTextureData[(y * width + x) * 4 + 1];
            row[x * 4 + 2] = this->_rawTextureData[(y * width + x) * 4 + 2];
            row[x * 4 + 3] = this->_rawTextureData[(y * width + x) * 4 + 3];
        }
        png_write_row(png, row);
    }

    // End write
    png_write_end(png, NULL);
    fclose(fp);
    png_free_data(png, info, PNG_FREE_ALL, -1);
    png_destroy_write_struct(&png, nullptr);
    free(row);
}
