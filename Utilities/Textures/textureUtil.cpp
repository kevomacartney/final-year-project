//
// Created by Kelvin Macartney on 10/12/2019.
//

#define STB_IMAGE_IMPLEMENTATION

#include <glm/gtc/matrix_transform.hpp>
#include "textureUtil.h"
#include "glad/glad.h"
#include <png.h>
#include <iostream>
#include <utility>
#include <map>

Utilities::Textures::TextureUtil::TextureUtil() {
    // initialises the map
    this->_textureMap = std::make_unique<std::map<int, std::shared_ptr<Textures::StripTexture>>>();
}

unsigned int Utilities::Textures::TextureUtil::generateTexture(std::weak_ptr<std::vector<Cell>> cells,
                                                               std::weak_ptr<Strip> strip) {
    auto texture = std::make_shared<StripTexture>(cells, strip);
    // Computes the texture from the data
    auto textureId = texture->computeTexture();

    // Register the texture with the utility
    this->_textureMap->insert(std::make_pair(textureId, std::move(texture)));
    return textureId;
}

std::shared_ptr<std::vector<glm::vec2>>
Utilities::Textures::TextureUtil::paintStripWithTexture(std::weak_ptr<Strip> strip) {
    auto lockedStrip = strip.lock();
    auto boundingBox = lockedStrip->_boundingBox;

    auto indices = std::make_shared<std::vector<glm::vec2>>();

    float scalingX{0}, scalingY{0};

    // Calculate the scaling factor
    scalingX = (float) (1.0f - 0.0f) / (boundingBox._xMax);
    scalingY = (float) (1.0f - 0.0f) / (boundingBox._yMax);

    // Scale to fit viewport
    auto scale = glm::scale(glm::mat4(1.0f),
                            glm::vec3(scalingX, scalingY, 0));

    // Transform every point
    for (auto v : *lockedStrip->_vertices) {
        auto point = v.Position;
        v.Position = scale * glm::vec4(point, 1);
        indices->push_back(glm::vec2(v.Position.x, v.Position.y));
    }

    return indices;
}


void Utilities::Textures::TextureUtil::activateTexture(unsigned int tId) {
    // activate the texture with the id
    this->_textureMap->at(tId)->use();
}

void Utilities::Textures::TextureUtil::deactivateTexture() {
    // Bind to no texture
    glBindTexture(GL_TEXTURE_2D, 0);
}

