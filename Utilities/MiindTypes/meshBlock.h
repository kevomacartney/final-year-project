#ifndef MESHBLOCK_H
#define MESHBLOCK_H

#include <glm/glm.hpp>
#include <vector>
#include <tuple>
#include <stdexcept>
#include <omp.h>
#include <iostream>
#include "vertex.h"
#include "../../defines.h"

class MeshBlock {
public:
    MeshBlock(size_t startAt) {
        startNo = startAt;
        _data = std::make_shared<std::vector<std::vector<PRECISION>>>();
        omp_init_lock(&this->_linesLock);

    }

    ~MeshBlock() {
        omp_destroy_lock(&this->_linesLock);
    }

    /**
     * Adds line data to the mesh block
     * @param lineData
     */
    void addLineData(std::vector<PRECISION> lineData) {
        this->_data->push_back(lineData);
    }

    /**
     * @brief GetIntergralCurve Retrieves the integral curve of the Kth pair
     * @param kthPair The start of the Kth vLine and wLine pair
     * @param step The ith step into the line
     * @return The vector containing the it's value
     */
    glm::vec2 getIntegralCurvePoint(size_t kthPair, size_t step) {
        if ((kthPair + 1 % 2) == 0)
            throw std::invalid_argument("Line number cannot be even");

        // Get the vLine and wLine
        auto xLine = GetLineData(kthPair);
        auto yLine = GetLineData(kthPair + 1);

        return glm::vec2(xLine.at(step), yLine.at(step));
    }

    /**
     * Returns all the lines for this block
     * @return
     */
    std::shared_ptr<std::vector<std::vector<Vertex>>> getLines(int meshIdx = 0) {
        omp_set_lock(&this->_linesLock);

        if (this->_lines == nullptr) {
            this->_lines = std::make_shared<std::vector<std::vector<Vertex>>>();
            this->_lines->reserve(this->_data->size());

            auto ogNo = startNo;
            // Iterate through the rows and columns
            for (u_long i = 0; i < this->_data->size(); i += 2) {
                std::vector<Vertex> line{};
                line.reserve(_data->at(i).size());

                for (u_long j = 0; j < this->_data->at(i).size(); ++j) {
                    auto linePoint = this->getIntegralCurvePoint(i, j);

                    // Add the line to the collection
                    line.push_back(Vertex(glm::vec3(linePoint, 0.f), startNo));
                    ++startNo;

                    // Find the lowest point in this mesh
                    if (linePoint.x < _boundingBox._xMin)
                        _boundingBox._xMin = linePoint.x;
                    if (linePoint.x > _boundingBox._xMax)
                        _boundingBox._xMax = linePoint.x;
                    if (linePoint.y < _boundingBox._yMin)
                        _boundingBox._yMin = linePoint.y;
                    if (linePoint.y > _boundingBox._yMax)
                        _boundingBox._yMax = linePoint.y;
                }

                // Add the line to the data
                this->_lines->push_back(line);
            }
            startNo = ogNo;
        }

        omp_unset_lock(&this->_linesLock);
        return this->_lines;
    }

    /**
     * Returns this meshes bounding box
     * @return
     */
    BoundingBox getBoundingBox(){
        return this->_boundingBox;
    }

private:

    /**
     * @brief Retrieves the line data for the line at kth position
     * @param kthBlock The block index
     * @return The Kth block location
     */
    std::vector<PRECISION> GetLineData(size_t kthLine) {
        return _data->at(kthLine);
    }

    // The meshes bounding box
    BoundingBox _boundingBox{};

    // Raw data from the mesh file
    std::shared_ptr<std::vector<std::vector<PRECISION>>> _data{nullptr};

    // Vertices created from the raw data
    std::shared_ptr<std::vector<std::vector<Vertex>>> _lines;

    // Lock for accessing line data
    omp_lock_t _linesLock{};

    // The starting number for numberiing the vertices
    size_t startNo;

};

#endif // MESHBLOCK_H
