//
// Created by Kelvin Macartney on 04/02/2020.
//

#ifndef MIIND_VERTEX_H
#define MIIND_VERTEX_H
#include <glm/glm.hpp>

struct Vertex{
public:
    Vertex()= default;
    explicit Vertex(glm::vec3 pos, size_t no){
        Position = pos;
        myNumber = no;
    }

    // The position of this point in space
    glm::vec3 Position{};

    // the number of this vertex
    size_t myNumber{0};
};


#endif //MIIND_VERTEX_H
