//
// Created by Kelvin Macartney on 20/03/2020.
//

#ifndef MIIND_BOUNDINGBOX_H
#define MIIND_BOUNDINGBOX_H

#include <climits>

struct BoundingBox {
public:
    float _xMin{INT_MAX};
    float _xMax{INT_MIN};
    float _yMin{INT_MAX};
    float _yMax{INT_MIN};
};

#endif //MIIND_BOUNDINGBOX_H
