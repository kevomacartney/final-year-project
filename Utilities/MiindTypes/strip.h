//
// Created by Kelvin Macartney on 23/02/2020.
//

#ifndef MIIND_STRIP_H
#define MIIND_STRIP_H

#include "vertex.h"
#include "boundingBox.h"
#include <vector>
#include <memory>

namespace Utilities::Textures {
    class TextureUtil;
}
struct Strip {
public:
    explicit Strip(std::shared_ptr<std::vector<Vertex>> vertices, std::shared_ptr<std::vector<int>> indices,
                   u_int32_t stripNo) :
            _vertices(std::move(vertices)),
            _vertexIndices(std::move(indices)),
            _stripNo(stripNo),
            _boundingBox(findBoundingBox()) {}

    // Top and bottom line
    std::shared_ptr<std::vector<Vertex>> _vertices{nullptr};

    // indices for this strip
    std::shared_ptr<std::vector<int>> _vertexIndices{};

    // texture uvs for this strip
    std::shared_ptr<std::vector<glm::vec2>> _textureUv{nullptr};

    // Texture utility
    std::shared_ptr<Utilities::Textures::TextureUtil> _textureUtil{nullptr};

    // Sets the texture identifier
    unsigned int _textureId{};

    // Strip number
    const int _stripNo{0};

    // The strips bounding box
    const BoundingBox _boundingBox;
private:
    BoundingBox findBoundingBox() {
        BoundingBox bBox = BoundingBox();
        for (auto vertex : *this->_vertices) {
            auto linePoint = vertex.Position;

            // Find the lowest point in this mesh
            if (linePoint.x < bBox._xMin)
                bBox._xMin = round(linePoint.x);
            if (linePoint.x > bBox._xMax)
                bBox._xMax = round(linePoint.x);
            if (linePoint.y < bBox._yMin)
                bBox._yMin = round(linePoint.y);
            if (linePoint.y > bBox._yMax)
                bBox._yMax = round(linePoint.y);
        }

        if (this->_stripNo == 376)
            return bBox;
        return bBox;
    }
};

#endif //MIIND_STRIP_H
