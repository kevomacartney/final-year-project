//
// Created by Kelvin Macartney on 10/03/2020.
//

#ifndef MIIND_CELL_H
#define MIIND_CELL_H

#include <vector>
#include "vertex.h"
#include "boundingBox.h"

struct Cell {
public:
    Cell(std::shared_ptr<std::vector<Vertex>> vertices, int number)
            : _vertices(std::move(vertices)),
              _cellNo(number),
              _cellArea(calculateCellArea()),
              _boundingBox(findBoundingBox()) {}

    Cell(std::shared_ptr<std::vector<Vertex>> vertices, int number, int mass)
            : _vertices(std::move(vertices)),
              _cellNo(number),
              cellMass(mass),
              _cellArea(calculateCellArea()) {

    }

    // Top and bottom line
    std::shared_ptr<std::vector<Vertex>> _vertices{nullptr};

    // indices for this strip
    std::shared_ptr<std::vector<int>> _indices{};

    // Gets the cell area
    const float _cellArea{0};

    // Gets the cells mass
    const float cellMass{0};

    // Gets the cell density
    float cellDensity() {
        return cellMass / _cellArea;
    }

    /**
     * Returns the cells bounding box
     * @return
     */
    BoundingBox getBoundBox() {
        return _boundingBox;
    }

    // cell number inside the parent strip
    const int _cellNo{0};

private:
    BoundingBox _boundingBox{};

    /**
     * Finds the bounding box of the given cell
     * @return The bounding box
     */
    BoundingBox findBoundingBox() {
        BoundingBox bBox = BoundingBox();
        for (auto vertex : *this->_vertices) {
            auto linePoint = vertex.Position;

            // Find the lowest point in this mesh
            if (linePoint.x < bBox._xMin)
                bBox._xMin = round(linePoint.x);
            if (linePoint.x > bBox._xMax)
                bBox._xMax = round(linePoint.x);
            if (linePoint.y < bBox._yMin)
                bBox._yMin = round(linePoint.y);
            if (linePoint.y > bBox._yMax)
                bBox._yMax = round(linePoint.y);
        }

        return bBox;
    }

    // Calculates the area of the cell
    float calculateCellArea() {
        float result{0};

        // Calculate the area
        for (long i = 0; i < _vertices->size() - 1; ++i) {
            auto vec1Pos = _vertices->at(i).Position;
            auto vec2Pos = _vertices->at(i + 1).Position;

            auto left = vec1Pos.x * vec2Pos.y;
            auto right = vec2Pos.x * vec1Pos.y;
            result += (left - right);
        }

        return result * .5f;
    }
};

#endif //MIIND_CELL_H
