//
// Created by Kelvin Macartney on 22/12/2019.
//

#ifndef MIIND_PROTOTYPE_VOAMODEL_H
#define MIIND_PROTOTYPE_VOAMODEL_H
#include "../Textures/textureUtil.h"

namespace Utilities::GlObjs {
    // Represents a single VOA object
    class VoaModel {
    public:
        VoaModel(unsigned int id, int vertexCount, bool indexed);

        // Retrieves the VOA id
        unsigned int getId();

        // Texture id of this object
        unsigned int textureId{};

        // Texture utili
        std::shared_ptr<Textures::TextureUtil> _textureUtil{nullptr};

        // Retrieves the vertices count
        int getVertexCount();

        // Gets a boolean indicating if EBO was used for this model
        bool isIndex();

        ~VoaModel();

    private:
        unsigned int _voaId{};
        int _vertexCount{};
        bool indexed{false};
    };
}

#endif //MIIND_PROTOTYPE_VOAMODEL_H
