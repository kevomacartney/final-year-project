//
// Created by Kelvin Macartney on 09/02/2020.
//

#include "meshRenderer.h"
#include "../Textures/textureUtil.h"
#include <utility>

void Utilities::Rendering::MeshRenderer::render(glm::mat4 projectionMatrix) {
    this->_shaderUtil->activate();
    if (this->_copyMatrices)
        this->copyMatrices(projectionMatrix);

    for (const auto &model : this->_renderQueue) {
        glBindVertexArray(model->getId());

        model->_textureUtil->activateTexture(model->textureId);

        if (model->isIndex())
            glDrawElements(GL_TRIANGLE_STRIP, model->getVertexCount(), GL_UNSIGNED_INT, 0);
        else
            glDrawArrays(GL_TRIANGLE_STRIP, 0, model->getVertexCount());

        model->_textureUtil->deactivateTexture();
    }


    this->_shaderUtil->deactivate();
    glBindVertexArray(0);
}

void Utilities::Rendering::MeshRenderer::copyMatrices(glm::mat4 projectionMatrix) {
    this->_shaderUtil->updateUniformValueMtx4("projection", projectionMatrix);

    this->_copyMatrices = false;
}

void
Utilities::Rendering::MeshRenderer::registerShaderUtil(std::shared_ptr<Utilities::Shaders::ShaderUtil> shaderUtil) {
    this->_shaderUtil = std::move(shaderUtil);
}

void
Utilities::Rendering::MeshRenderer::addToRenderQueue(const std::shared_ptr<Utilities::GlObjs::VoaModel> &lineVoaModel) {
    this->_renderQueue.push_back(lineVoaModel);
}
