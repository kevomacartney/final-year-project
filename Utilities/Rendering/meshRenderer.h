//
// Created by Kelvin Macartney on 09/02/2020.
//

#ifndef MIIND_MESHRENDERER_H
#define MIIND_MESHRENDERER_H
#include <memory>
#include <utility>
#include <vector>
#include "renderer.h"
#include "renderModel.h"
#include "../GlObjs/voaModel.h"
#include "../Shaders/shaderUtil.h"

namespace Utilities::Rendering {
    class MeshRenderer : public BaseRenderer {
    public:
        MeshRenderer() = default;
        ~MeshRenderer() = default;

        // Renders the lines registered to this renderer
        void render(glm::mat4 projectionMatrix) override;

        // Registers the shader device to be used by this renderer
        void registerShaderUtil(std::shared_ptr<Utilities::Shaders::ShaderUtil> shaderUtil);

        // Adds a line to the render queue
        void addToRenderQueue(const std::shared_ptr<Utilities::GlObjs::VoaModel>& lineVoaModel);
    private:
        std::vector<std::shared_ptr<Utilities::GlObjs::VoaModel>> _renderQueue{};
        std::shared_ptr<Utilities::Shaders::ShaderUtil> _shaderUtil {};

        void copyMatrices(glm::mat4 projectionMatrix);

        bool _copyMatrices{true};
    };
}


#endif //MIIND_MESHRENDERER_H
