//
// Created by Kelvin Macartney on 18/12/2019.
//

#ifndef MIIND_PROTOTYPE_BASICCAMERA_H
#define MIIND_PROTOTYPE_BASICCAMERA_H

#include <glm/glm.hpp>

namespace Managers::Window {
    class MainWindow ;
}
namespace Camera {
    class BasicCamera {
    public:
        explicit BasicCamera( Managers::Window::MainWindow *window, float closePlane, float farPlane, int width, int height);

        ~BasicCamera() = default;

        // Gets the projection-matrix
        glm::mat4 getProjectionMatrix(){
            return this->projectionMatrix;
        }

        // Calculates the view matrix
        void calculateMatrices();

        // Update the viewport height and width
        void updateViewport(int w, int h);

    protected:
        // The projection matrix
        glm::mat4 projectionMatrix{};

    private:
        // The height and width of the window
        int _height{}, _width{};
        // Close and far plane of the pyramid
        float _closePlane{}, _farPlane{};

    };
}


#endif //MIIND_PROTOTYPE_BASICCAMERA_H
