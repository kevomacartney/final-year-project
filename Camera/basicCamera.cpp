//
// Created by Kelvin Macartney on 18/12/2019.
//

#include <glm/gtc/matrix_transform.hpp>
#include "../Managers/Window/mainWindow.h"
#include "basicCamera.h"

Camera::BasicCamera::BasicCamera(Managers::Window::MainWindow *window, float closePlane, float farPlane, int width,
                                 int height) {
    this->_height = height;
    this->_width = width;

    this->_closePlane = closePlane;
    this->_farPlane = farPlane;

    this->projectionMatrix = glm::mat4(1.f);
}

void Camera::BasicCamera::updateViewport(int w, int h) {
    this->_width = w;
    this->_height = h;
}

void Camera::BasicCamera::calculateMatrices() {
    this->projectionMatrix = glm::ortho(0.f,
                                        (float) this->_width,
                                        0.f,
                                        (float) this->_height,
                                        -1.f, 10.f);
}
